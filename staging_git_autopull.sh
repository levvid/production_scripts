#!/usr/bin/env bash


while :  # while true
do
    cd ~;
    cd scripts;
    git pull origin master
    echo "Done pulling from scripts"
    sleep 1m  # sleep for 1 minute
    cd ~;
    cd Code/lis;
    echo "Pulling from origin staging..."
    git pull origin staging
    echo "Git pull execution done"
    echo "Pulling from scripts"
    sleep 12h  # pull every 12 hours
done