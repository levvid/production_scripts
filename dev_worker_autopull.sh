#!/usr/bin/env bash


while :  # while true
do
    cd ~;
    cd scripts;
    git pull origin master
    echo "Done pulling from scripts"
    sleep 1m  # sleep for 1 minute
    cd ~;
    cd Code/lis;
    echo "Pulling from origin staging..."
    git pull origin staging
    echo "Git pull execution done"
    echo "Pulling from scripts"
    #sleep 5m  # wait 5 minutes.
    # restart workers
    # default queue
    # start default worker
    source ~/.virtualenvs/lis/bin/activate && celery multi start -A main worker -l info -Q default -c4 --pidfile=~/.virtualenvs/lis/bin/celery%n.pid
    # identifiers queue
    # start identifiers worker
    source ~/.virtualenvs/lis/bin/activate && celery multi start -A main worker -Q identifiers --concurrency=1 -l info -c4 --pidfile=/home/ubuntu/.virtualenvs/lis/bin/celery%n.pid
    # restart identifiers worker
    source ~/.virtualenvs/lis/bin/activate && celery multi restart 1 --pidfile=/home/ubuntu/.virtualenvs/lis/bin/celery%n.pid
    sleep 1h  # sleep 1hr
done