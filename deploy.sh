#!/usr/bin/env bash

# Dev server - openvpnas@ec2-54-236-53-74.compute-1.amazonaws.com
# Prod server - openvpnas@ec2-3-229-132-104.compute-1.amazonaws.com
LOG=logfile.txt
#set -x # echo

# server vars
SERVER='ssh -tt -i "DendiOpenVPNServer.pem" openvpnas@ec2-54-236-53-74.compute-1.amazonaws.com'
DEV_PI='pi@dendidev.dendi.com'
PROD_PI='pi@ildp.dendi.com'
RASPBERRY_PI=$DEV_PI

# git vars
GIT_PULL='git pull origin staging'
GIT_CHECKOUT='git checkout staging'

# save output of command to LOG
# runthis "<command>"
runthis(){
    ## print the command to the logfile
    echo "$@" >> $LOG
    ## run the command and redirect it's error output
    ## to the logfile
    eval "$@" 2>> $LOG
}


echo -e "Do you want to deploy to Development or Production Raspberry PI's?  \n\t 1. Production  \n\t 2. Development \
\n [1 or 2]? Default 2 - Development\n"
read environment

if [ $environment -eq 1 ]; then
    echo -e "Production selected...\n\n"
    SERVER="ssh -tt -i "DendiOpenVPNServer.pem" openvpnas@ec2-3-229-132-104.compute-1.amazonaws.com"
    #echo $SERVER
else
    echo -e "Development selected...\n\n"
    #echo $SERVER
fi

echo -e "Do you want to pull from Staging or Master \n\t 1. Master  \n\t 2. Staging \
\n [1 or 2]? Default 2 - Staging\n"
read user_git_branch

if [ $user_git_branch -eq 1 ]; then
    echo -e "Branch master selected...\n\n"
    GIT_BRANCH='git pull origin master'
    GIT_CHECKOUT='git checkout master'
    RASPBERRY_PI=$PROD_PI
    #echo $RASPBERRY_PI
    #echo $GIT_PULL
else
    echo -e "Branch staging selected...\n\n"
    GIT_CHECKOUT='git checkout staging'
    #echo $GIT_PULL
fi


cd ~
eval $SERVER <<END_OPENVPN_SERVER
    # on OPENVPN Server. SSHing to Dendi Dev PI...
    sleep 2
    sshpass -p "dendipower" ssh -tt -o StrictHostKeyChecking=no $RASPBERRY_PI <<END_DENDI_DEV
        # On Dendi Dev PI.
        sleep 2
        cd ~/lis;
        # checkout relevant branch
        eval $GIT_CHECKOUT
        # pull from branch
        eval $GIT_PULL
        sleep 1
        # Done with git pull. Exiting...
        sleep 2
        exit
END_DENDI_DEV
exit
END_OPENVPN_SERVER

#set +x
