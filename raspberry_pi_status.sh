#!/usr/bin/env bash

HOSTS="50.50.50.10"
SECONDS=0
COUNT=4
raspberry_client_up=true


for myHost in $HOSTS
do
count=$(ping -c $COUNT $myHost | grep 'received' | awk -F',' '{ print $2 }' | awk '{ print $1 }')

while true
do
  # run ping forever
  if [ $count -eq 0 ]; then
    # 100% failed
    if [ "$raspberry_client_up" != false ] ; then
      # client is down, send email and set status to is_down
      # only send the email once
      echo "Server failed at $(date)" | mail -s "VPN Server Down" gmulonga@dendisoftware.com
      echo "Host : $myHost is down (ping failed) at $(date)"
      raspberry_client_up=false
    fi
  else
    # client is up
    if [ "$raspberry_client_up" == false ] ; then
      duration=$SECONDS
      echo "Host: $myHost is now up. It was down for $(($duration / 60)) minutes and $(($duration % 60)) seconds"
      SECONDS=0
    fi
    raspberry_client_up=true
    echo "Host : $myHost is up..."
  fi
  done

done
