#!/usr/bin/env bash

#cd ~
# ssh to the openvpn server
# ssh -i "DendiOpenVPNServer.pem" openvpnas@ec2-54-236-53-74.compute-1.amazonaws.com
# use sshpass to ssh to a raspberry pi
# sshpass -p "dendipower" ssh -o StrictHostKeyChecking=no pi@dendidev.dendi.com
# -tt on ssh - force pseudo-tty allocation even if stdin isn't a terminal

set -x # echo

cd ~
ssh -tt -i "DendiOpenVPNServer.pem" openvpnas@ec2-54-236-53-74.compute-1.amazonaws.com \
'./scripts/pi_ssh_script.sh -release'